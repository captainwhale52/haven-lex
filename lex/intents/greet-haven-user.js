'use strict';

// const profanity = require('profanity-util');

const builder = require('../builder');


function validateUsername(username) {
    // if (username && profanity.check(username).length > 0) {
    //     return builder.validationBuilder(false, 'username', `I don't think ${username} is your name. Tell me your real name.`, null);
    // }
    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    console.log(intentRequest.currentIntent.slots);
    const username = intentRequest.currentIntent.slots['username'];
    const source = intentRequest.invocationSource;
    console.log(`source: ${source}`);
    console.log(intentRequest.sessionAttributes);

    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validateUsername(username);

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));

    } else if (source === 'FulfillmentCodeHook') {
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }
        intentRequest.sessionAttributes['username'] = username;
        return Promise.resolve(
            builder.switchIntent(intentRequest.sessionAttributes, 'SelectHavenExperience', 'experience', { experience: null },
                builder.messageBuilder(`Welcome to Haven ${username}. I got 3 suggestions for you today. Heavenly body, Waterfall, and Space Dream. Which one would you like to try?`)
        ));
    }
};
