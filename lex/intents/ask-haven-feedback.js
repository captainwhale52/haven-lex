'use strict';

const sentiment = require('sentiment');

const builder = require('../builder');

let isLiked = 'like';

function validateFeedback(experience, feedback) {
    // if (username && profanity.check(username).length > 0) {
    //     return builder.validationBuilder(false, 'username', `I don't think ${username} is your name. Tell me your real name.`, null);
    // }

    if (!feedback || feedback === '') {
        return builder.validationBuilder(false, 'feedback', `The experience has finished. How do you like it?`, null);
    }
    console.log(`Raw feedback message: ${feedback}`);
    feedback = feedback.replace(`didn't`, `don't`);
    feedback = feedback.replace('did', 'don');
    console.log(sentiment(feedback));
    if (sentiment(feedback).score < 0) {
        isLiked = 'dislike';
    }

    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    const username = intentRequest.currentIntent.slots['username'];
    const experience = intentRequest.currentIntent.slots['experience'];
    const feedback = intentRequest.currentIntent.slots['feedback'];
    const source = intentRequest.invocationSource;

    console.log(`source: ${source}`);
    console.log(intentRequest.currentIntent.slots);
    console.log(intentRequest.sessionAttributes);

    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validateFeedback(experience, feedback);

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));

    } else if (source === 'FulfillmentCodeHook') {
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }

        if (isLiked === 'like') {
            intentRequest.sessionAttributes['username'] = username;
            intentRequest.sessionAttributes['experience'] = experience;
            intentRequest.sessionAttributes['feedback'] = 'like';
            return Promise.resolve(builder.close(
                intentRequest.sessionAttributes, 'Fulfilled',
                builder.messageBuilder(`I am glad you enjoyed the ${experience} experience. I hope you have a great rest of the day.`, null)
            ));
        } else {
            intentRequest.sessionAttributes['username'] = username;
            intentRequest.sessionAttributes['experience'] = experience;
            intentRequest.sessionAttributes['feedback'] = 'dislike';
            return Promise.resolve(builder.close(
                intentRequest.sessionAttributes, 'Fulfilled',
                builder.messageBuilder(`Thanks for your feedback. I hope to see you again.`, null)
            ));
        }

    }
};
