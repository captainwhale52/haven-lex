'use strict';

const greetHavenUser = require('./intents/greet-haven-user');
const selectHavenExperience = require('./intents/select-haven-experience');
const askHavenFeedback = require('./intents/ask-haven-feedback');


module.exports = function(intentRequest) {
    console.log(`dispatch userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);
    const intentName = intentRequest.currentIntent.name;

    if (intentName === 'GreetHavenUser') {
        console.log(`${intentName} is called`);
        return greetHavenUser(intentRequest);
    }

    if (intentName === 'SelectHavenExperience') {
        console.log(`${intentName} is called`);
        return selectHavenExperience(intentRequest);
    }

    if (intentName === 'AskHavenFeedback') {
        console.log(`${intentName} is called`);
        return askHavenFeedback(intentRequest);
    }

    throw new Error(`Intent with a name ${intentName} is not supported`);
};
